def peca(a): #retorna a peça que corresponde ao nḿero de entrada
    return "Peão" * (a == 1) + "Bispo" * (a == 2) +\
           "Cavalo" * (a == 3) + "Torre" * (a == 4) +\
           "Rei" * (a == 5) + "Rainha" * (a == 6)


def count(num, matrix): #retorna a ocorrencia de um número em uma matriz
    total = 0
    for array in matrix:
        total += array.count(num)
    return total


tabuleiro = [[4, 3, 2, 5, 6, 2, 3, 4],
             [1, 1, 1, 1, 1, 1, 1, 1],
             [0, 0, 0, 0, 0, 0, 0, 0],
             [0, 0, 0, 0, 0, 0, 0, 0],
             [0, 0, 0, 0, 0, 0, 0, 0],
             [0, 0, 0, 0, 0, 0, 0, 0],
             [1, 1, 1, 1, 1, 1, 1, 1],
             [4, 3, 2, 5, 6, 2, 3, 4]]


for i in range(1, 7):
    pec = peca(i)
    quantidade = count(i, tabuleiro)

    print("{} : {}".format(peca(i), count(i, tabuleiro)))
    print("\n **Quantidade inválida da peça {}!!** \n".format(pec) * 
          ((pec == "Bispo" or pec == "Cavalo" or pec == "Torre") and quantidade > 4
           or pec == "Peão" and quantidade > 16
           or (pec == "Rei" or pec == "Rainha") and quantidade > 2))